#!/usr/bin/python
# -*- coding: utf-8 -*-
#Autor: Karl-Erik Krais
#Osa koodi on võetud *1* Pythoni kontrolltööst ja vajadusel muudetud. *2*Osa funktsioonist visitUrl()- http://stackoverflow.com/questions/4925966/searching-through-webpage
#Kirjeldus:Skript saab kaks käsurea parameetrit
    #1. Serveri nimi või IP aadress
    #2. Failinimi, kus sees on URLi serverile järgnev osa ja otsitav string.
    #Skript teeb käsurea parameetriga antud serveri pihta HTTP GET päringuid, mis on teise argumendina antud failis ja otsib vastusena saadud lehelt samas failis antud otsitavat stringi.
    #Kui string leitakse, kirjutatakse väljundisse:
    #URL,otsitavstring,kellaaeg(aasta-kuu-päev_tund-minut-sekund),OK või NOK

import sys
import os
import re
import urllib2
import datetime
 
filtered_lines = []

# *1* Funktsioon, mis eemaldab ära tühjad (\n) 
def filterLines():
    lines = openFile.readlines()
    #Itereerime kõik read läbi
    for idx,line in enumerate(lines):
        line = line.strip("\n")
        #Loome dictionary
        data = {}
        data['url'] = line.split(",")[0]
        data['search'] = line.split(",")[1:]
        #Lisame dictionary eraldatud väärtused
        filtered_lines.insert(idx, data)
 
# *2* Funktsioon, mis otsib lehelt stringi ja kuvab vastuse terminali  
def visitUrl():
    for line in filtered_lines:
        url = serverName + line['url']
        try:
            html_content = urllib2.urlopen(url).read()
            result = re.findall(str(line['search'][0]), html_content)
        except urllib2.HTTPError, e:
            result = re.findall(str(line['search'][0]), e.read())
        if len(result) == 0:
            response = "NOK"
        else:
            response = "OK"
        print "%s,%s,%s,%s" % (url, line['search'][0], datetime.datetime.now().strftime("%Y-%M-%d_%H-%M-%S"), response)
 
#Kontrollime, kas käsurea parameetreid on 2:
if len(sys.argv) != 3:
    print "Vale kasutus!"
    sys.exit()
   
#Salvestame kasutaja sisestatud parameetrid muutujatesse:
 serverName = sys.argv[1]
fileName = sys.argv[2]
 
#Kontrollime, kas kõige esimene rida sisaldab stringi "http" ja "www", kui mitte siis lisame
if serverName[0][0:4] != "http":
    if serverName[0][7:10] != "www":
        serverName = "www." + serverName 
    serverName = "http://" + serverName

#Kas sisendfail eksisteerib:
if os.path.exists(fileName):
    file = open(fileName, "r")
    file.close()
    openFile = open(fileName)
    #Käivitame funktsioonid
    filterLines()
    visitUrl()
else:
    print "Sisendfail ei eksisteeri"
    sys.exit()
 
